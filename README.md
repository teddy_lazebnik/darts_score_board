# Home Darts Score Board

A Website allowing to manage scores of darts game with multiple players!

This is not a fancy project, just something I made for myself as we have a darts game at our house and we spend too much paper to count score.

This project increased a little bit, allowing us and our friends to play darts as part of a set of game and keep scores. In addition, as one of us cannot remember the scoring system there is a "help" window making it visual to calc scores. 

Lastly, as sometimes is nice to play when each one adds it's score to the board, there is a shared game option (using Flask-SocketIO).

Hope you will enjoy it for your darts games!

## Using the website

1. Install the requirements
2. Run "main.py"
3. Go to your local host (usually:  http://127.0.0.1:5000/)
4. Have fun
5. for shared game sending email feature - connect your email account

## Useful improvements

1. Remove the Flask server component, use just JS for the system (It will make it easier to use for more people). For the shared game you will need a server-side.
2. Add a DB remembering historical games. (Currently, remembering results but not drill down scores)
3. Make the design better.
4. Allow to send WhatsApp \ SMS upon game shared.