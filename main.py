import json
import random
import string
from models import Game
from logic.darts_game import DartsGame
from flask_socketio import SocketIO, emit, join_room, leave_room
from logic.darts_player import DartsPlayer
from database import db, create_app_for_db
from utils.email_sender_gmail import EmailSenderGmail
from flask import Flask, request, render_template, make_response, redirect, url_for, jsonify, session

app = create_app_for_db()

app.config['SECRET_KEY'] = "vnkdjnfjknfl1232#"
socketio = SocketIO(app)

shared_games = []

try:
    with open("settings.json", "r") as settings_file:
        settings = json.load(settings_file)
except Exception as error:
    settings = {"EMAIL": {"email": "", "password": ""}}
    print("Cannot load settings because: {}".format(error))

# consts #
domain = "127.0.0.5000"
join_entrypoint = "shared_game"
# end - consts #


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


@app.route("/", methods=["GET", "POST"])
def new_game():
    try:
        players = request.cookies.get('players')
        if players is None or request.args.get('new_game', default='0') == '1':
            return render_template("index.html")
        else:
            return redirect(url_for("game"))
    except Exception as error:
        res = make_response(render_template("index.html"))
        return res.set_cookie("players", value="")


@app.route("/shared_game_new", methods=["GET", "POST"])
def shared_game_new():
    return render_template("shared_game_new.html")


@app.route("/game", methods=["GET", "POST"])
def game():
    try:
        players_cookie = request.cookies.get('players')
        if players_cookie is None or request.form.get('player1', "") != "":
            players = []
            for counter in range(1, 7):
                name = request.form.get('player{}'.format(counter), "")
                if name != "":
                    players.append(fixed_name_to_be_unqie(names=players, new_name=name))
        else:
            players = players_cookie.split(",")
        res = make_response(render_template("game.html",
                                            players_count=len(players),
                                            players=players,
                                            card_size=round(12/len(players))))
        res.set_cookie('players', ",".join(players))
        return res
    except Exception as error:
        return redirect(url_for("new_game"))


@app.route("/shared_game", methods=["GET", "POST"])
def shared_game():
    try:
        if request.args.get('join', default="") != "":
            return join_game(name=request.args.get('name', default=''),
                             game_id=request.args.get('game', default=''))

        game_id = random_game_id()
        players = []
        darts_players = []
        for counter in range(1, 7):
            name = request.form.get('name{}'.format(counter), "").strip().lower()
            email = request.form.get('email{}'.format(counter), "").strip().lower()
            if name != "" and (email != "" or counter == 1):
                players.append(fixed_name_to_be_unqie(names=players, new_name=name))
                darts_players.append(DartsPlayer(name=name,
                                                 email=email,
                                                 is_join=True))
                # send an email to each member of the game (except the admin which is the first one)
                if counter != 1:
                    darts_players[counter-1].disconnect()
                    EmailSenderGmail.send_email_via_gmail(to=email,
                                                          subject="You have been invited for home darts game",
                                                          text="Your unique link to join the game is: http://{}/{}?game={}&name={}&join=1"
                                                          .format(domain, join_entrypoint, game_id, name))
        res = make_response(render_template("shared_game.html",
                                            players_count=len(players),
                                            players=players,
                                            name=players[0],
                                            game_id=game_id,
                                            player_index=0,
                                            card_size=round(12/len(players))))
        # tell each player the game id
        res.set_cookie('shared_game_id', game_id)
        global shared_games
        shared_games.append(DartsGame(id=game_id,
                                      players=darts_players))
        return res
    except Exception as error:
        return redirect(url_for("shared_game_new"))


def join_game(name: str = "",
              game_id: str = ""):
    """
    Join player to exsiting game game
    """
    try:
        if name == "" or game_id == "":
            raise Exception("Data not provided")
        this_game = None
        for current_shared_game in shared_games:
            if current_shared_game.id == game_id:
                this_game = current_shared_game
                for player in this_game.players:
                    if player.get_name() == name.strip().lower():
                        player.join()
                break
        if this_game is None:
            raise Exception("Game not found")
        res = make_response(render_template("shared_game.html",
                                            players_count=this_game.get_players_count(),
                                            name=name,
                                            player_index=this_game.get_player_index(name=name.strip().lower()),
                                            game_id=game_id,
                                            players=[player.get_name() for player in this_game.players],
                                            card_size=round(12 / this_game.get_players_count())))
        # tell each player the game id
        res.set_cookie('shared_game_id', game_id)
        return res
    except Exception as error:
        return redirect(url_for("shared_game_new"))


@app.route("/history", methods=["GET", "POST"])
def history():
    """
    Show the history of the games
    :return: shows the list of historical games
    """
    history = Game.query.all()
    return render_template("history.html",
                           history_games=[json.loads(game_row.record) for game_row in history],
                           played_date=[game_row.created_at.strftime("%d/%m/%Y, %H:%M") for game_row in history])


@app.route("/history_summery", methods=["GET", "POST"])
def history_summery():
    """
    Show the history of the game for 2 players only
    :return: shows the list of historical games
    """
    history = [game_row for game_row in Game.query.all() if "teddy" in game_row.record.lower() and "liza" in game_row.record.lower()]
    games_values = [list(json.loads(game_row.record).values()) for game_row in history]
    winners = [0 if int(game_value[0]) > int(game_value[1]) else 1 for game_value in games_values]
    liza_score = sum(winners)
    teddy_score = len(winners) - liza_score
    return render_template("history_summery.html",
                           history_games=games_values,
                           winners=winners,
                           liza_score=liza_score,
                           teddy_score=teddy_score,
                           played_date=[game_row.created_at.strftime("%d/%m/%Y, %H:%M") for game_row in history])


# --- ajax calls --- #

@app.route("/add_game", methods=["GET", "POST"])
def add_game():
    """ add a game to the DB """
    if request.method == 'POST':
        # Exclude Next Key
        dict_data = request.form
        # Store Data in Table
        exp = Game(record=json.dumps(dict_data))
        db.session.add(exp)
        db.session.commit()
        return jsonify("saved")  # Return Response

# events of a shared game
# 1.a. player join the game (input)
# 1.b. let everybody already joined to know the status of players in the game (output)
# 2.a. player enter a score (input)
# 2.b. let everybody know that a player enter a score and this is the next move
# 2.c. if game over, let everybody know that the game is over and who is the winner.


@socketio.on('joined')
def joined(json_input, methods=['GET', 'POST']):
    game_id = json_input["game_id"]
    this_game = get_game_by_id(game_id)
    # if no such game, publish empty project
    if this_game is not None:
        join_player = this_game.get_player_by_name(name=json_input["name"].strip().lower())
        if join_player is None:
            json_input["name"] = ""
        else:
            join_player.join()
        join_room(game_id)
        return emit('join',
                    {"game_id": game_id, "names": this_game.get_joined_players_names()},
                    room=game_id)
    return emit('join',
                {"game_id": "", "names": []})


@socketio.on('add_score')
def add_score(json_input, methods=['GET', 'POST']):
    game_id = json_input["game_id"]
    this_game = get_game_by_id(game_id)
    # if no such game, publish empty project
    if this_game is not None:
        this_game.add_score(json_input["score"])
        return emit('publish_new_score',
                    {"game_id": game_id, "score": json_input["score"]},
                    room=game_id)


# --- end - ajax calls --- #

# --- help functions --- #


def fixed_name_to_be_unqie(names: list,
                           new_name: str) -> str:
    """
    Make sure the new name is uniqe in the list
    :param names: list of names we already have
    :param new_name: the new name we wish to add
    :return: the name in a uniqe format
    """
    if new_name not in names:
        return new_name
    counter = 1
    while "{} ({})".format(new_name, counter) in names:
        counter += 1
    return "{} ({})".format(new_name, counter)


def get_game_by_id(game_id: str) -> DartsGame:
    """
    Find the game according to it's id
    :param game_id: the game's id
    :return: the game online if exsits
    """
    for current_shared_game in shared_games:
        if current_shared_game.id == game_id:
            return current_shared_game
    return None


def random_game_id(length: int = 8) -> str:
    """
    generates a random string in a given length
    :param length: the length of the string
    :return: a random string
    """
    letters = string.ascii_letters + string.digits
    return "".join(random.choice(letters) for i in range(length))

# --- end - help functions --- #


if __name__ == "__main__":
    # run process
    socketio.run(app)
