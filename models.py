import enum
from datetime import datetime
from database import create_app_for_db
from flask_sqlalchemy import SQLAlchemy

# consts #
db = SQLAlchemy(create_app_for_db())
# end - consts #


class Game(db.Model):
    __tablename__ = 'game'

    id = db.Column(db.Integer, primary_key=True)
    record = db.Column(db.String(500), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)


if __name__ == '__main__':
    db.create_all()
