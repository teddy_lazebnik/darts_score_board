from flask import Flask
from flask_sqlalchemy import SQLAlchemy


class DisconnectHandlingSQLAlchemy(SQLAlchemy):
    def apply_driver_hacks(self, app, info, options):
        super().apply_driver_hacks(app, info, options)

        options["pool_pre_ping"] = True


def create_app_for_db():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///games.sqlite3'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['DEBUG'] = False
    db.init_app(app)
    return app


db = DisconnectHandlingSQLAlchemy()
