import json
import os
import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

try:
    with open("settings.json", "r") as settings_file:
        settings = json.load(settings_file)
except Exception as error:
    settings = {"EMAIL": {"email": "", "password": ""}}
    print("Cannot load settings because: {}".format(error))


class EmailSenderGmail:
    """
    SMTP email sender using Google's API
    """

    # ---> CONSTS <--- #

    GMAIL_SMTP_URL = "smtp.gmail.com"
    GMAIL_SMTP_PORT = 587

    GMAIL_USER = settings["EMAIL"]["email"]
    GMAIL_PASSWORD = settings["EMAIL"]["password"]

    # ---> END - CONSTS <--- #

    def __init__(self):
        pass

    @staticmethod
    def send_email_via_gmail(to, subject, text):
        """ Send an email to someone with given title and text using the Gmail API SMTP """
        try:
            final_text = subject + "\n\n\n" + text.replace("[", "").replace("]", "").replace("'", "")
            message = MIMEMultipart()

            message['From'] = EmailSenderGmail.GMAIL_USER
            message['To'] = to
            message['Subject'] = "Email from Associator system"
            message.attach(MIMEText(final_text, "plain"))

            session = smtplib.SMTP(EmailSenderGmail.GMAIL_SMTP_URL, EmailSenderGmail.GMAIL_SMTP_PORT)
            session.starttls()
            session.login(EmailSenderGmail.GMAIL_USER, EmailSenderGmail.GMAIL_PASSWORD)

            text = message.as_string()
            session.sendmail(EmailSenderGmail.GMAIL_USER, to, text.encode("utf8"))
            session.quit()

            return None

            server = smtplib.SMTP_SSL(
                EmailSenderGmail.GMAIL_SMTP_URL, EmailSenderGmail.GMAIL_SMTP_PORT
            )
            server.ehlo()
            # login to my gmail account
            server.login(
                EmailSenderGmail.GMAIL_USER,
                EmailSenderGmail.GMAIL_PASSWORD,
            )
            # send the email itself
            server.sendmail(
                EmailSenderGmail.GMAIL_USER, to, final_text.encode("utf8")
            )
            # close connection
            server.close()
        except Exception as error:
            print("Could not get the Gmail SMTP connection cause: {}".format(error))

    @staticmethod
    def send_email_via_gmail_with_file(to, subject, text, attach_file_path):
        """ Send an email with a file to someone with given title and text using the Gmail API SMTP  """
        try:
            message = MIMEMultipart()
            message['From'] = EmailSenderGmail.GMAIL_USER
            message['To'] = to
            message['Subject'] = subject
            message.attach(MIMEText(text, 'plain'))
            payload = MIMEBase('application', 'octate-stream')
            payload.set_payload((open(attach_file_path, 'rb')).read())
            encoders.encode_base64(payload)
            payload.add_header('Content-Decomposition', 'attachment', filename=os.path.basename(attach_file_path))
            message.attach(payload)
            session = smtplib.SMTP('smtp.gmail.com', 587)
            session.starttls() #enable security
            session.login(EmailSenderGmail.GMAIL_USER, EmailSenderGmail.GMAIL_PASSWORD)
            text = message.as_string()
            session.sendmail(EmailSenderGmail.GMAIL_USER, to, text)
            session.quit()
        except ConnectionError:
            print("Could not get the Gmail SMTP connection to send email with file")

    @staticmethod
    def send_email_via_gmail_list(to_list, subject, text):
        """ Send an email to list of people with given title and text using the Gmail API SMTP """
        for to_email in to_list:
            EmailSenderGmail.send_email_via_gmail(to_email, subject, text)
