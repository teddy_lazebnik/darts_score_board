class DartsPlayer:
    """
    A darts player class managing all the aspects of the player
    """

    def __init__(self,
                 name: str,
                 email: str,
                 is_join: bool):
        self._name = name
        self._email = email
        self._is_join = is_join
        self._scores = []

    def last_index(self) -> int:
        """
        :return the length of the scores list
        """
        return len(self._scores)

    def get_email(self) -> str:
        return self._email

    def get_name(self) -> str:
        return self._name

    def is_connect(self) -> bool:
        return self._is_join

    def total_score(self) -> int:
        """
        :return the sum of scores
        """
        return sum(self._scores)

    def join(self) -> None:
        self._is_join = True

    def disconnect(self) -> None:
        self._is_join = False

    def add_score(self,
                  new_score: int) -> None:
        """
        add a new score to the player
        :param new_score: the score to add
        :return: none
        """
        if new_score >= 0:
            self._scores.append(new_score)

    def remove_score(self,
                     score_index: int) -> None:
        """
        remove a score from the player by index
        :param score_index: the score's index to remove
        :return: none
        """
        if 0 <= score_index < len(self._scores):
            self._scores.remove(self._scores[score_index])

    def remove_last_score(self) -> None:
        """
        remove the last score from the player by index
        :return: none
        """
        self._scores.remove(self._scores[-1])

    def edit_score(self,
                   score_index: int,
                   new_score: int) -> None:
        """
        remove a score from the player by index
        :param score_index: the score's index to remove
        :param new_score: the score to add
        :return: none
        """
        if 0 <= score_index < len(self._scores):
            self._scores[score_index] = new_score

    def reset(self) -> None:
        """
        reset the scores
        """
        self._scores.clear()

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return "<DartsPlayer: name={}, email={}>".format(self.get_name(), self.get_email())
