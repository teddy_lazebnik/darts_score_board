from logic.darts_player import DartsPlayer


class DartsGame:
    """
    A class to manage all the logic of a darts game, including players on boarding and status
    """

    def __init__(self,
                 id: str,
                 players: list):
        self.id = id
        self.players = players
        self.this_turn = 0

    def get_players_count(self) -> int:
        return len(self.players)

    def get_joined_players_names(self) -> list:
        return [player.get_name() for player in self.players if player.is_connect()]

    def get_player_by_name(self,
                           name: str) -> DartsPlayer:
        for player in self.players:
            if player.get_name().strip().lower() == name:
                return player
        return None

    def get_player_index(self,
                         name: str) -> int:
        for index, player in enumerate(self.players):
            if player.get_name() == name:
                return index
        return -1

    def add_score(self,
                  new_score: int) -> int:
        self.players[self.this_turn % len(self.players)].add_score(new_score=new_score)
        return self.players[self.this_turn % len(self.players)].total_score()

    def remove_last_score(self):
        self.this_turn -= 1
        self.players[self.this_turn % len(self.players)].remove_last_score()

    def edit_score(self,
                   player_index: int,
                   score_index: int,
                   new_score: int) -> None:
        self.players[player_index].edit_score(score_index=score_index,
                                              new_score=new_score)

    def reset(self) -> None:
        self.this_turn = 0
        [self.players[i].reset() for i in range(len(self.players))]

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        answer = ""
        for player in self.players:
            answer += "{},".format(player)
        return "<DartsGame: players={}>".format(answer)
