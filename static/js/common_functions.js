function change_theme()
{
    try
    {
        if (theme == "dark")
        {
            $("#body").css("background-color", "white");
            $("#table_header").css("background-color", "#a9a8b5");
            $("#table_body").css("background-color", "#ececec");
            $("#table_header").css("color", "black");
            $("#title").css("color", "black");
            $("#table_body").css("color", "black");
            $("#table_body").css("color", "black");
            $("#table_body").css("color", "black");
            $("#add_btn_score").css("color", "black");
            $("#new_game_btn").css("color", "black");
            $("#table_tr_header").css("color", "black");

            $("#theme_btn").html("<i class='fas fa-tint'></i>");
            theme = "light";
        }
        else
        {
            $("#body").css("background-color", "#454d55");
            $("#table_header").css("background-color", "#821923");
            $("#table_body").css("background-color", "#9a7913");
            $("#table_header").css("color", "white");
            $("#title").css("color", "white");
            $("#table_body").css("color", "white");
            $("#add_btn_score").css("color", "white");
            $("#new_game_btn").css("color", "white");
            $("#table_tr_header").css("color", "white");

            $("#theme_btn").html("<i class='fas fa-tint-slash'></i>");
            theme = "dark";
        }
        $("#scoreValue").focus();

        return false;
    }
    catch(error) {
      console.error(error);
      window.theme = "dark";
      change_theme();
    }
}


function startTime()
{
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById("clock").innerHTML =
  h + ":" + m + ":" + s;
  var t = setTimeout(startTime, 1000);
}

function checkTime(i)
{
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}

function distance(first_point_x, first_point_y, second_point_x, second_point_y)
{
    return Math.sqrt(Math.pow((second_point_x - first_point_x), 2) + Math.pow((second_point_y - first_point_y), 2))
}

function vector_norm(x, y)
{
    return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2))
}

function calcAngleDegrees(y, x)
{
    return radiansToDegrees(calcAngle(y, x));
}

function calcAngle(y, x)
{
    if (x > 0 && y > 0)
    {
        return Math.atan(y / x);
    }
    else if (x < 0)
    {
        return Math.PI + Math.atan(y / x);
    }
    else
    {
        return 2 * Math.PI + Math.atan(y/x);
    }
}

function radiansToDegrees(angle)
{
    return angle / Math.PI * 180;
}

function DegreesToRadians(angle)
{
    return angle / 180 * Math.PI;
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
