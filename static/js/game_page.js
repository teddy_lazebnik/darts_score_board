

$(document).ready(function() {
    startTime();
    try
    {
        set_max_score(window.winScore);
    }
    catch (error)
    {
        console.log(error);
    }
    $("img").on("click", function(event) {
        // get center of image location
        var x = event.pageX;
        var y = event.pageY;
        if (window.board_center_x == 0 && window.board_center_y == 0)
        {
            window.board_center_x = x;
            window.board_center_y = y;
            $('#help_comment').hide();
            return;
        }
        var point_position_x = x - window.board_center_x;
        var point_position_y = y - window.board_center_y;
        var angle = calcAngleDegrees(-1 * point_position_y, point_position_x);
        var dist = vector_norm(point_position_x, point_position_y);
        console.log("Hit relative to the center at x=" + point_position_x + " , y=" + point_position_y + "");
        console.log("Angle=" + angle + " , Dist=" + dist + "");
        var nextScore = 0;
        if (dist < 10)
        {
            nextScore = 100;
        }
        else if (dist < 20)
        {
            nextScore = 50;
        }
        else
        {
            if (angle <= 9 && angle > 0)
            {
                nextScore = 6;
            }
            else if (angle <= 27 && angle > 9)
            {
                nextScore = 13;
            }
            else if (angle <= 45 && angle > 27)
            {
                nextScore = 4;
            }
            else if (angle <= 63 && angle > 45)
            {
                nextScore = 18;
            }
            else if (angle <= 81 && angle > 63)
            {
                nextScore = 1;
            }
            else if (angle <= 99 && angle > 81)
            {
                nextScore = 20;
            }
            else if (angle <= 117 && angle > 99)
            {
                nextScore = 5;
            }
            else if (angle <= 135 && angle > 117)
            {
                nextScore = 12;
            }
            else if (angle <= 153 && angle > 135)
            {
                nextScore = 9;
            }
            else if (angle <= 171 && angle > 153)
            {
                nextScore = 14;
            }
            else if (angle <= 189 && angle > 171)
            {
                nextScore = 11;
            }
            else if (angle <= 207 && angle > 189)
            {
                nextScore = 8;
            }
            else if (angle <= 225 && angle > 207)
            {
                nextScore = 16;
            }
            else if (angle <= 243 && angle > 225)
            {
                nextScore = 7;
            }
            else if (angle <= 261 && angle > 243)
            {
                nextScore = 19;
            }
            else if (angle <= 279 && angle > 261)
            {
                nextScore = 3;
            }
            else if (angle <= 297 && angle > 279)
            {
                nextScore = 17;
            }
            else if (angle <= 315 && angle > 297)
            {
                nextScore = 2;
            }
            else if (angle <= 333 && angle > 315)
            {
                nextScore = 15;
            }
            else if (angle <= 351 && angle > 333)
            {
                nextScore = 10;
            }
            else if (angle <= 360 && angle > 351)
            {
                nextScore = 6;
            }

            // the outer rings
            if (dist >= 85 && dist <= 95)
            {
                nextScore *= 3;
            }
            else if (dist >= 142 && dist <= 152)
            {
                nextScore *= 2;
            }
            else if (dist > 152)
            {
                nextScore = 0;
            }
        }
        console.log("Score=" + nextScore);
        // add the values to keeping
        window.running_help_score += nextScore;
        $("#help_score").text("Score: " + window.running_help_score);
        if (window.running_help_score > window.winScore - 1)
        {
            alert("Illegal value, cannot throw more then " + (window.winScore - 1) + " points");
            window.running_help_score = window.winScore - 1;
            $("#help_score").text("Score: " + window.running_help_score);
        }

        try{
            open_close_score();
       }
       catch (error){
       }
    });
});

$('.input').keypress(function (e) {
      if (e.which == 13) {
        e.preventDefault();
        try{
        add_score();
        }
        catch (error)
        {
            console.log(error);
        }
        return false;
      }
    });

$("#scoreValue").focus();

function set_max_score(new_max)
{
    window.winScore = new_max;
    for (var i = 0; i < window.legitWinScores.length; i++)
    {
        document.getElementById("max_score_" + window.legitWinScores[i]).innerHTML = "" + window.legitWinScores[i];
    }
    document.getElementById("max_score_" + new_max).innerHTML = "<i class='fas fa-heart'></i> " + new_max + " <i class='fas fa-heart'></i>";
    setCookie("maxScore", new_max, 7);

    for (var i = 0; i < window.game.player_count(); i++)
    {
        var left = (window.winScore - window.game.players[i].total_score());
        if (left < 0)
        {
            left = 0;
        }
        $("#player_score_" + i).html("<i class='fas fa-map-marker-alt'></i> Score: " + window.game.players[i].total_score() + " <span class='lower'>(Left: " + left + ")</span>");
    }
}

function add_score()
{
    var score = parseInt($("#scoreValue").val());

    if (score >= 0 && score < window.winScore)
    {
        var totalScore = window.game.add_score(score);
        let left = (window.winScore - totalScore);
        if (left < 0)
        {
            left = 0;
        }
        $("#player_score_" + window.game.player_turn()).html("<i class='fas fa-map-marker-alt'></i> Score: " + totalScore + " <span class='lower'>(Left: " + left + ")</span>");
        if (score > 199)
        {
            $("#circle" + window.game.circle_number()).append("<td><i class='fas fa-rocket'></i>  " + score + "  <i class='fas fa-rocket'></td>");
        }
        else if (score > 99)
        {
            $("#circle" + window.game.circle_number()).append("<td><i class='far fa-smile'></i>  " + score + "  <i class='far fa-smile'></i></td>");
        }
        else if (score < 11)
        {
            $("#circle" + window.game.circle_number()).append("<td><i class='far fa-sad-tear'></i>  " + score + "  <i class='far fa-sad-tear'></i></td>");
        }
        else
        {
            $("#circle" + window.game.circle_number()).append("<td>" + score + "</td>");
        }
        $("#scoreValue").val(0);
        window.game.thisTurn += 1;
        if (window.sound == "play")
        {
            if (score > 199)
            {
                document.getElementById("addAudioVeryHappy").play();
            }
            else if (score >= 100)
            {
                document.getElementById("addAudioHappy").play();
            }
            else if (score <= 20)
            {
                document.getElementById("addAudio020").play();
            }
            else if (score <= 40)
            {
                document.getElementById("addAudio2040").play();
            }
            else if (score <= 60)
            {
                document.getElementById("addAudio4060").play();
            }
            else if (score == 69)
            {
                document.getElementById("addAudio69").play();
            }
            else if (score <= 80)
            {
                document.getElementById("addAudio6080").play();
            }
            else if (score < 100)
            {
                document.getElementById("addAudio80100").play();
            }
        }
    }
    else
    {
        return false;
    }

    is_winner = window.game.is_winner(window.winScore);
    if (window.game.is_end_of_circle())
    {
        $("#table_body").append("<tr id='circle" + window.game.circle_number() + "'></tr>");

        if (is_winner)
        {
            var winPlayerIndex = window.game.who_leading();
            window.game.add_winner(winPlayerIndex);
            $("#win_alert").html("<i class='fas fa-trophy'></i> Player '" + window.game.get_player_name(winPlayerIndex) + "' is the winner <i class='fas fa-trophy'></i> ");
            $('#winAlert').modal('show');
            if (window.game.get_player_wins(winPlayerIndex) > 0)
            {
                $("#player_game_won_" + winPlayerIndex).text("Games Won: " + window.game.get_player_wins(winPlayerIndex));
            }
            $("#end_panel").show();
            $("#players").hide();
            if (window.sound == "play")
            {
                document.getElementById("winAudio").play();
            }
            submit_game();
        }
        else
        {
            var bestPlayerIndex = 0;
            var bestScore = 0;

            for (var i = 0; i < window.game.player_count(); i++)
            {
                userScore = window.game.get_player_score(i);
                if (userScore > bestScore)
                {
                    bestScore = userScore;
                    bestPlayerIndex = i;
                }
                $("#leading" + i).hide();
            }
            $("#leading" + bestPlayerIndex).show();
        }
    }

    for (var i = 0; i < window.game.player_count(); i++)
    {
        $("#bag" + i).hide();
    }
    if (!is_winner)
    {
        $("#bag" + window.game.player_turn()).show();
    }

    $("#scoreValue").focus();

    return false;
}

function undo_move()
{
    if (window.game.thisTurn == 0)
    {
        return false;
    }

    window.game.remove_last_score();
    for (var i = 0; i < window.game.player_count(); i++)
    {
        $("#bag" + i).hide();
        $("#leading" + i).hide();
    }
    $("#bag" + window.game.player_turn()).show();

    for (var i = 0; i < window.game.player_count(); i++)
    {
        var totalScore = window.game.players[i].total_score();
        let left = (window.winScore - totalScore);
        if (left < 0)
        {
            left = 0;
        }
        $("#player_score_" + i).html("<i class='fas fa-map-marker-alt'></i> Score: " + totalScore + " <span class='lower'>(Left: " + left + ")</span>");
    }

    $("#circle" + window.game.circle_number()).children().last().remove();
    console.log("clear_element");
    if ((window.game.thisTurn % window.game.players.length) == 0)
    {
        $("#table_body").append("<tr id='circle" + window.game.circle_number() + "'></tr>");
    }

    $("#scoreValue").focus();

    return false;
}

function reset_game()
{
    for (var i = 0; i < window.game.player_count(); i++)
    {
        $("#bag" + i).hide();
    }
    $("#bag0").show();

    for (var i = 0; i < window.game.player_count(); i++)
    {
        $("#player_score_" + i).html("<i class='fas fa-map-marker-alt'></i> Score: 0 <span class='lower'>(Left: 501)</span>");
    }

    $("#table_body").empty();
    $("#table_body").append("<tr id='circle0'></tr>");

    window.game.reset();

    $("#end_panel").hide();
    $("#players").show();
    $("#scoreValue").focus();

    return false;
}

function mute_play()
{
    if (window.sound == "mute")
    {
        $("#mute_play_btn").html("<i class='fas fa-volume-up'></i>");
        window.sound = "play";
    }
    else
    {
        $("#mute_play_btn").html("<i class='fas fa-volume-mute'></i>");
        window.sound = "mute";
    }

    $("#scoreValue").focus();
}

function close_alert()
{
    $('#winAlert').modal('hide');
}

function open_help()
{
    window.running_help_score = 0;
    $("#help_score").text("Score: " + window.running_help_score);
    $('#help_model').modal('show');
}

function open_settings()
{
    $('#settingsWindow').modal('show');
}

function close_settings()
{
    $('#settingsWindow').modal('hide');

    $("#scoreValue").focus();
}

function finish_help()
{
    $("#scoreValue").val(window.running_help_score);
    $('#help_model').modal('hide');
    add_score();

    $("#scoreValue").focus();
}

function submit_game()
{
    let formData = new FormData();
    for (var i = 0; i < window.game.player_count(); i++)
    {
        formData.append(window.game.get_player_name(i), parseInt(window.game.get_player_score(i)));
    }
    $.ajax({ // Ajax Call to Store Data into Database.
        url: '/add_game',
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function(response) {

        },
        error: function(error) {
            alert(JSON.stringify(error));
        }
    });
}