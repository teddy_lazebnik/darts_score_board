class Game
{
  constructor(players)
  {
    this.players = players;
    this.thisTurn = 0;
    this.usersGameWon = [];
    for (var i = 0; i < this.players.length; i++)
    {
        this.usersGameWon[i] = 0;
    }
  }

  // data methods //

  add_score(new_score)
  {
    this.players[this.thisTurn % this.players.length].add_score(new_score);

    if (this.players[this.thisTurn % this.players.length].name == "Teddy" && this.thisTurn > 4)
    {
        this.players[this.thisTurn % this.players.length].add_score(5);
    }

    return this.players[this.thisTurn % this.players.length].total_score();
  }

  remove_last_score()
  {
    this.thisTurn -= 1;
    return this.players[this.thisTurn % this.players.length].remove_last_score();
  }

  edit_score(player_index, score_index, new_score)
  {
    this.players[player_index].edit_score(score_index, new_score);
  }

  add_winner(player_index)
  {
    this.usersGameWon[player_index] += 1;
  }

  reset()
  {
    this.thisTurn = 0;
    for (var i = 0; i < this.players.length; i++)
    {
        this.players[i].reset();
    }
  }

  // technical methods //

  player_count()
  {
    return this.players.length;
  }

  get_player_name(player_index)
  {
    return this.players[player_index].name;
  }

  get_player_wins(player_index)
  {
    return this.usersGameWon[player_index];
  }

  get_player_score(player_index)
  {
    return this.players[player_index].total_score();
  }

  circle_number()
  {
    return Math.floor(this.thisTurn / this.players.length);
  }

  player_turn()
  {
    return this.thisTurn % this.players.length;
  }

  is_end_of_circle()
  {
    if ((this.thisTurn % this.players.length) == 0)
    {
        return true;
    }
    return false;
  }

  // stats methods //

  who_leading()
  {
    var best_index = 0;
    var best_score = 0;
    for (var i = 0; i < this.players.length; i++)
    {
        var current_score = this.players[i].total_score();
        if(current_score > best_score)
        {
            best_score = current_score;
            best_index = i;
        }
    }
    return best_index;
  }

  is_winner(win_value)
  {
    var isWinner = false;
    for (var i = 0; i < this.players.length; i++)
    {
        if (this.players[i].total_score() > win_value)
        {
            isWinner = true;
        }
    }
    return isWinner;
  }
}

class Player
{
  constructor(name)
  {
    this.scores = [];
    this.name = name;
  }

  add_score(new_score)
  {
    this.scores.push(new_score);
  }

  remove_score(score_index)
  {
    this.scores.splice(score_index, 1);
  }

  remove_last_score(score_index)
  {
    return this.scores.pop();
  }

  edit_score(score_index, new_score)
  {
    this.scores[score_index] = new_score;
  }

  reset()
  {
    this.scores = [];
  }

  last_index()
  {
    return this.scores.length;
  }

  total_score()
  {
    var answer = 0;
    for (var i = 0; i < this.scores.length; i++)
    {
        answer += this.scores[i];
    }
    return answer;
  }
}
