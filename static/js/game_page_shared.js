$('.input').keypress(function (e) {
      if (e.which == 13) {
        e.preventDefault();
        add_score_shared();
        return false;
      }
});

function finish_help_shared()
{
    $("#scoreValue").val(window.running_help_score);
    $('#help_model').modal('hide');
    add_score_shared();
}

function add_score_shared()
{
    if (!window.active_turn)
    {
        return false;
    }
    console.log("Add score event. Send: score = " + $("#scoreValue").val());
    socket.emit('add_score', {
        game_id: window.game_id,
        score: parseInt($("#scoreValue").val())
    });
    return false;
}

function open_close_score(){
    if ((window.game.thisTurn % window.game.players.length) != window.player_index || window.game.players.length != window.palyers_connected)
    {
        window.active_turn = false;
        document.getElementById("scoreValue").disabled = true;
    }
    else{
        window.active_turn = true;
        document.getElementById("scoreValue").disabled = false;
    }
}